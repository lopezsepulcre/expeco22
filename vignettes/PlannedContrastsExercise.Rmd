---
title: "Planned Contrasts"
author: "Andrés López-Sepulcre"
date: "3/23/2021"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### Background

This data is from an experiment where we wanted to know the effects of two diffenrent fish: guppies *Poecilia reticulata*, and Hart's killifish *Anablepsoides hartii*, on the algal biomass of streams. While guppies are diurnal, killifish are mainly nocturnal. To do so we set an experiment in natural streams with 4 types of plots:

- **E** = Electrified exclosures that excluded both fish
- **C** = Non electrified exclosures that allowed both fish to go in
- **D** = Exclosures that were only electrified during the day, when guppies forage
- **N** = Exclosures that were only electrified during the night, when killifish forage

We want to ask:

1. What is the effect of guppies on algal biomass?
2. What is the effect of killifish on algal crops?
3. Is their joint effect just the sum of both effects or is there some kind of interaction?

The data is in `TriniStreamEnclosure.txt`.

```{r}
trinidad <- read.table('../data/TriniStreamExclosure.txt', header = T)
```

We can hacve a look at the data first

```{r}
plot(algal.crop ~  treatment,data=trinidad,
     xlab = 'Treatment',
     ylab = 'Algal biomass (gChla/m2)')
```

We could simply analyze the data with a linear model assuming normal errors

```{r}
modelT1<-glm(algal.crop ~  treatment,data=trinidad, family='gaussian')

knitr::kable(summary(modelT1)$coefficients)
```

By default, `R` sets a model where the intercept is the treatment that goes first in alphabetical order, and the effects are the difference between a given treatment and that intercept.

This is done by creating 3 dummy variables that take value of 0 or 1 if the data come from a given treatment.

This can be seen from the contrasts (or design) matrix

```{r}
contrasts(trinidad$treatment)
```

With **C** being the only treatment with 0 value for all three dummy variables (**D**,**E**, and **N**), and thus it is the intercept (the prediction for which all effects are 0).

The way to read this is that `R` created 3 variables (columns), called **D**, **E** and **N**, and each takes a value of 0 or 1 depending on the treatment (rows).So, for example, variable **D** will take a value of 1 if the treatment is **D** and zero otherwise

The model is:

$$
\text{algal biomass} = intercept+ \beta_1  \cdot \textbf{D}+\beta_2 \cdot \textbf{E}+\beta_3 \cdot \textbf{N}
$$

If the treatment is **C**, all variables take the value of zero, and therefore the prediction is simply:

$$
\text{algal biomass} = intercept + \beta_1 \cdot 0 + \beta_2 \cdot 0+ \beta_3 \cdot 0 \\

\text{algal biomass} = 0
$$

Hence, the intercept is the estimate for the control (non electrified) plots.

We can change the matrix so that we have different contrasts. For examplem, we may want to compare everything to treatment **E**, because that is the plot with NO fish access and this could be the baseline for calculating our effecta.

We do this like this:

```{r}
new.contrast<-cbind(
  C=c(1,0,0,0),
  D=c(0,1,0,0),
  N=c(0,0,0,1))
row.names(new.contrast)=c('C','D',"E",'N')
```

Let's have a look at it

```{r}
new.contrast
```

Now we assign this new contrast to the vairable treatment

```{r}
contrasts(trinidad$treatment)<-new.contrast
```

And if we run the model again, we will see that the comparisons made are the ones I indicated

```{r}
modelT2<-glm(algal.crop ~  treatment,data=trinidad, family='gaussian')
knitr::kable(summary(modelT2)$coefficients)
```

**TASK** Design a new contrast matrix that answers the three questions above. (3 vairables, 3 contrasts, 3 questions) and tests whether those effects are significantly different from zero. Assign those new contrasts and run the model that tests them.

**HINT** It is always useful to think of the linear equation behind the model, what each of the parameters do and, most importanrt, what would be the prediction for each scenario. Note that you don't always need to build dummy variables/contrasts where only one factor level takes the value of 1.
