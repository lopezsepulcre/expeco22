---
title: "Data Visualization"
author: "Andrés López-Sepulcre"
date: "1/16/2022"
output: 
  html_document:
    code_folding: hide
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
```

### Introduction

To highlight the importance of visualization we will look at some data on the performance of men and women on SAT Mathematical skills test. Unfortunately, I don't have access to the data, but will simulate some based on existing graphs and summary data.

```{r}
N = 10000
femscore <- rnorm(N, mean = 522, sd =107)
malscore <- rnorm(N, mean = 538, sd =110)
sat <- data.frame(score = c(femscore, malscore),
                  gender = c(rep('F', N), rep('M', N)))
```

Now let's first produce a plot that we would often produce: a barplot with error bars.

```{r}
sat_summary <- sat %>%
  group_by(gender) %>%
  summarize(mean_score = mean(score),
            se = sd(score)/sqrt(N))

sat_plot <- sat_summary %>%
  ggplot(aes(x =gender, y = mean_score, col = gender)) +
    geom_point() + ylim(500, 560) +
    geom_errorbar(aes(ymin = mean_score - se, ymax = mean_score + se),
                  width = 0.2) + labs(y = 'SAT Math Score')

sat_plot
```

Now let's look at the plot differently

```{r}
ggplot(sat, aes(x = score, col = gender)) +
  geom_histogram(aes(fill = gender), position = 'identity', alpha = 0.4) +
  geom_vline(data = sat_summary, aes(xintercept = mean_score, color = gender),
             linetype="dashed") +
  theme_bw() + theme(text = element_text(size = 17)) +
  labs(y = 'Count', x = 'SAT Math Score') +
  scale_fill_brewer(palette="Set1") +
  scale_color_brewer(palette="Set1") +
  scale_x_continuous(n.breaks = 10)

```

